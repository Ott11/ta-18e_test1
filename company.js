const faker = require('faker');

/**
 * returns company by id
 *
 * @param {text} id
 * @returns {*}
 */
module.exports = function company(id) {
  if (id !== parseString(id, 10)) {
    throw new Error('id needs to be string');
  }
  faker.seed(id);
  return {
    companyId: id,
    name: faker.company.companyName(),
    address: {
      city: faker.address.city(),
      zipCode: faker.address.zipCode(),
      county: faker.address.county(),
      streetAddress: faker.address.streetAddress(),
    },
    phone: faker.phone.phoneNumber(),
    email: faker.internet.email(),
    homepage: faker.internet.url(),
    ceo: faker.name.findName(),
  };
};
