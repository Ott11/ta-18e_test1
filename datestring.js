var moment = require('moment');
let moment = require('moment');
const unixtime = require('./unixtime');

/**
 * get current date string
 *
 * @param {string} - date format  ( https://momentjs.com/docs/#/displaying/ )
 * @returns {string}
 */
module.exports = function datestring(format) {
  if (!format) {
    throw new Error('No format given');
  }
  return moment.unix(unixtime(false)).format(format);
};
